# region
provider "aws" {
  region = "us-east-2"
}

# crearte S3 backet to store remote stata
resource "aws_s3_bucket" "ex1" {
  bucket = "my-terraform-s3-state-demo-bucket"
}

# create dynamodb table for lock state
resource "aws_dynamodb_table" "my-terraform-s3-state-lock-demo-table" {

  name = "my-terraform-s3-state-lock-demo-table"

  # add 20 different entries, 20 state locks
  read_capacity = 20
  write_capacity = 20

  hash_key = "lockID"

  attribute {
    name = "lockID"
    type = "S"
  }

  tags = {
    Name = "DynamoDB state lock table"
  }

}

